import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-logs-view',
  templateUrl: './logs-view.component.html',
  styleUrls: ['./logs-view.component.css'],
})
export class LogsViewComponent implements OnInit, OnDestroy {
  logs: any;
  displayModal = false;
  currentLog: any;
  intervalId: any;

  constructor() {}

  async ngOnInit() {
    const fetchResponse = await fetch('http://localhost:9090/history');
    const responseObject = await fetchResponse.json();
    if (localStorage.getItem('cameraLogs') === null) {
      this.logs = responseObject;
      localStorage.setItem('cameraLogs', JSON.stringify(this.logs));
    } else {
      this.logs = JSON.parse(localStorage.getItem('cameraLogs'));
    }

    this.intervalId = setInterval(async () => {
      const fetchResponse = await fetch('http://localhost:9090/logs');
      const responseObject = await fetchResponse.json();

      const prevLogs = JSON.parse(localStorage.getItem('cameraLogs'));
      prevLogs.push(...responseObject);
      prevLogs.sort((a, b) => {
        return new Date(b.date).getTime() - new Date(a.date).getTime();
      });
      this.logs = prevLogs;

      localStorage.setItem('cameraLogs', JSON.stringify(this.logs));
    }, 10000);
  }

  ngOnDestroy() {
    clearInterval(this.intervalId);
  }

  public sortByDueDate(): void {
    this.logs.sort((a, b) => {
      return a.date.getTime() - b.date.getTime();
    });
  }

  onImgClick(value: string) {
    this.currentLog = this.logs.filter((log) => log.id === parseInt(value))[0];
    this.displayModal = true;
  }

  onCloseClick() {
    this.displayModal = false;
    this.currentLog = undefined;
  }
}
